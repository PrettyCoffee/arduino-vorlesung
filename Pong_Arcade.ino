#include <RGBmatrixPanel.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>


//Game Properties
#define BOARD_HEIGHT 8
#define BOARD_WIDTH 1

//Anschlüsse initialisieren
//Input Player 1
#define Y_AXIS_P1 A8
#define X_AXIS_P1 A9
#define BUTTON_P1_PIN 18
//#define MENU_BUTTON_PIN 44
//Input Player 2
#define Y_AXIS_P2 A10
#define X_AXIS_P2 A11
#define BUTTON_P2_PIN 20

int BUTTON_P1 = 0;
int BUTTON_P2 = 0;

/********************************************************************
 * Class Definitions                                                *
 ********************************************************************/
 class Menu {
 private:
    int menuMode = 0; //0 == MainMenu; 1 == StartGame; 2 == Settings;
    int selectedEntry = 0;

    //Show Menu depending on menuMode
    void mainMenu();        //menuMode == 0
    void startGame();       //menuMode == 1
    void settingsMenu();    //menuMode == 2
    void drawSelection();
 public:
    Menu();
    //Print active Menu
    void print();
    //Move selection
    //direction 0==up; 1==down;
    void moveSelection(int direction); 
    //Execute Selection Action
    void action();
 };

class Player {
private:
    int score = 0;
    const int playerNum;
public:
    int board[2]; //board coordinates
    Player(int playerNum);
    void resetBoard(); //reset board position
    void moveBoard(int direction); // Move player Board, direction: -1=none; 0=up; 1=down;
    void drawScore(); //Update score
    void addPoint(); //add 1 to score
    int getScore();
};

class Ball {
private:
    double pos[2] = { 1, 16 };
    int angle = 60;
    int angleSwitchLock = 0;
public:
    int direction = 0;
    void move();
    bool isAtXBorder();
    bool isAtYBorder();
    bool hasHitBoardOfPlayer(Player* player);
    void calcNewAngle(Player* player);
    int getPosY();
    void resetBall(int playernum);
};


/********************************************************************
 * Global Variables                                                 *
 ********************************************************************/

 //Output
RGBmatrixPanel matrix(A0, A1, A2, A3, 11, 10, 9, false, 64);

//Colors
const uint16_t WHITE = matrix.Color333(7, 7, 7);
const uint16_t BLACK = matrix.Color333(0, 0, 0);
const uint16_t RED = matrix.Color333(7, 0, 0);

//mode = 0 -> Menu; mode = 1 -> Pong
enum ArcadeState { menu, pong };

ArcadeState mode = menu;
Player* player1;
Player* player2;
Ball* ball;
Menu* menuRef;

//Settings Variables
int npc = 0;
int npcDifficulty = 3; //je größer npcDifficulty, je schwerer wird der NPC Gegner. Sorgt dafür, dass sich der npc bei zB 3 in 1/3 Fällen bewegt pro Loop (4 -> 1/4; 5 -> 1/5 etc)
int winScore = 3; //Score to reach
int speed = 1;

/*
 * Executes once before loop()
 *************************************/
void setup() {
    matrix.begin();

    pinMode(BUTTON_P1_PIN, INPUT);
    pinMode(BUTTON_P2_PIN, INPUT);

    //Set Text Settings
    matrix.setTextColor(WHITE);
    matrix.setTextSize(1);// size 1 == 7 pixels high ...ABER 1 != 7 WTF WAS LOS BEI DIR LERN MATHE
    attachInterrupt(digitalPinToInterrupt(BUTTON_P1_PIN), interruptButton1, HIGH);
    attachInterrupt(digitalPinToInterrupt(BUTTON_P2_PIN), interruptButton2, HIGH);
}

//Button Interrupts
void interruptButton1() {
    BUTTON_P1 = 1;
}
void interruptButton2() {
    BUTTON_P2 = 1;
}

/*
 * Repeats until exit
 *************************************/
void loop() {
    /*
    * Menu Loop
    ***************/
    if (mode == menu) {
        matrix.fillScreen(BLACK);
        menuRef = new Menu();
        do {
            BUTTON_P1 = digitalRead(BUTTON_P1_PIN);
            BUTTON_P2 = digitalRead(BUTTON_P2_PIN);
            //Move Menu Selection
            if (analogRead(Y_AXIS_P1) > 540) {
                menuRef->moveSelection(0);
                delay(150);
            } else if (analogRead(Y_AXIS_P1) < 500) {
                menuRef->moveSelection(1);
                delay(150);
            }

            //Execute Action
            if (BUTTON_P1 == 1)
                menuRef->action();
                BUTTON_P1 = 0;
        } while (mode == menu);
    }
    /*
    * Pong Game Loop
    ***************/
    else if (mode == pong) {
        matrix.fillScreen(BLACK);
        bool gameOver = false;
        int counter = 0;
        //Player declaration
        player1 = new Player(1);
        player2 = new Player(2);
        ball = new Ball();  //in Konstruktor: zufälliger anfangswinkel!
        //initiate game
        player1->resetBoard();
        player2->resetBoard();
        player1->drawScore();
        player2->drawScore();
        matrix.fillRect(31, 0, 2, 32, WHITE);
        delay(3000);

        do {
            BUTTON_P1 = digitalRead(BUTTON_P1_PIN);
            BUTTON_P2 = digitalRead(BUTTON_P2_PIN);
            counter++;
            //Pong Movement
            ball->move();
            delay(3*(9-speed));

            //Board Movement
            //Player 1
            if (npc < 2) {
                if (analogRead(Y_AXIS_P1) < 500 && counter%3 == 0)
                    player1->moveBoard(1);
                else if (analogRead(Y_AXIS_P1) < 150 && counter%2 == 0)
                    player1->moveBoard(1);
                else if (analogRead(Y_AXIS_P1) < 50)
                    player1->moveBoard(1);
                    
                else if (analogRead(Y_AXIS_P1) > 540  && counter%3 == 0)
                    player1->moveBoard(0);
                else if (analogRead(Y_AXIS_P1) > 800 && counter%2 == 0)
                    player1->moveBoard(0);
                else if (analogRead(Y_AXIS_P1) > 950)
                    player1->moveBoard(0);
            } else {
                if (ball->direction == 1 && counter%npcDifficulty != 0) {
                    if (ball->getPosY() <= (player1->board[1]+2))
                        player1->moveBoard(0);
                    else if (ball->getPosY() >= (player1->board[1] + BOARD_HEIGHT-2))
                        player1->moveBoard(1);
                }
            }

            //Player 2
            if (npc == 0){
                if (analogRead(Y_AXIS_P2) < 475 && counter%3 == 0)
                    player2->moveBoard(0);
                else if (analogRead(Y_AXIS_P2) < 200 && counter%2 == 0)
                    player2->moveBoard(0);
                else if (analogRead(Y_AXIS_P2) < 100)
                    player2->moveBoard(0);

                else if (analogRead(Y_AXIS_P2) > 520  && counter%3 == 0)
                    player2->moveBoard(1);
                else if (analogRead(Y_AXIS_P2) > 780 && counter%2 == 0)
                    player2->moveBoard(1);
                else if (analogRead(Y_AXIS_P2) > 920)
                    player2->moveBoard(1);
            }
            //NPC Steuerung
            else {
                if (ball->direction == 0 && counter%npcDifficulty != 0) {
                    if (ball->getPosY() <= (player2->board[1]+2))
                        player2->moveBoard(0);
                    else if (ball->getPosY() >= (player2->board[1] + BOARD_HEIGHT-2))
                        player2->moveBoard(1);
                }
            }

            //redraw middle line
            matrix.fillRect(31, 0, 2, 32, WHITE);

            //Has somebody won the game?
            if (player1->getScore() >= winScore || player2->getScore() >= winScore) {
                gameOver = true; //<- game ends
                //who won the game
                char winner[] = "Player  ";
                if (player1->getScore() >= winScore)
                    winner[7] = '1';
                else
                    winner[7] = '2';

                //show winner and wait
                matrix.fillScreen(BLACK);
                matrix.setCursor(12, 8);
                matrix.print(winner);
                matrix.setCursor(12, 18);
                matrix.print("won!");
                delay(4000);
            }
            //End Game
            if (BUTTON_P1 == 1 || BUTTON_P2 == 1 || gameOver) {
                //CleanUp
                delete player1;
                delete player2;
                delete ball;
                mode = menu;
                BUTTON_P1 = 0;
                BUTTON_P2 = 0;
            }
        } while (mode == pong);
    }
}


/********************************************************************
 * Class Implementations                                            *
 ********************************************************************/

 /*
  *Menu
  *************/

Menu::Menu() {
    this->print();
}
void Menu::print() {
    matrix.fillRect(0, 0, 64, 32, BLACK);
    switch (this->menuMode) {
        case 0: //Main Menu
            this->mainMenu();
        break;
        case 1: //Start Game
            this->startGame();
        break;
        case 2: //Settings
            this->settingsMenu();
        break;
    }
    this->drawSelection();
}
void Menu::mainMenu() {
    matrix.setCursor(2, 2);
    matrix.print("Menu");
    matrix.setCursor(8, 11);
    matrix.print("Start");
    matrix.setCursor(8, 20);
    matrix.print("Settings");
}
void Menu::startGame(){
    matrix.setCursor(8, 2);
    matrix.print("0Player");
    matrix.setCursor(8, 11);
    matrix.print("1Player");
    matrix.setCursor(8, 20);
    matrix.print("2Player");
}
void Menu::settingsMenu() {
    matrix.setCursor(8, 2);
    matrix.print("Diff:");
    matrix.print(npcDifficulty);
    matrix.setCursor(8, 11);
    matrix.print("Score:");
    matrix.print(winScore);
    matrix.setCursor(8, 20);
    matrix.print("Speed:");
    matrix.print(speed);
}
void Menu::drawSelection () {
    //remove old selection
    switch (this->menuMode) {
        case 0: //Main Menu
            matrix.fillRect(2,11,5,20,BLACK);
        break;
        case 1: //Start Game
            matrix.fillRect(2,2,5,30,BLACK);
            matrix.fillRect(50,23,15,12,BLACK);
        break;
        case 2: //Settings
            matrix.fillRect(2,2,5,30,BLACK);
            matrix.fillRect(50,23,15,12,BLACK);
        break;
    }

    //draw new selection
    switch (this->selectedEntry) {
        case -1:
            matrix.setCursor(2, 2);
        break;
        case 0:
            matrix.setCursor(2, 11);
        break;
        case 1:
            matrix.setCursor(2, 20);
        break;
        case 2:
            matrix.setCursor(50,23);
            matrix.setTextColor(RED);
        break;
    }

    //Print Selection
    if (this->selectedEntry < 2)
        matrix.print(">");
    else {
        matrix.print(">>");
        matrix.setTextColor(WHITE);
    }
}
//direction 0==up; 1==down;
void Menu::moveSelection(int direction) {
    matrix.fillRect(2,11,5,20,BLACK);
    //Move up if pressed
    switch (this->menuMode) {
        case 0:
            if (direction == 0 && selectedEntry > 0)
                selectedEntry--;
            else if (direction == 1 && selectedEntry < 1)
                selectedEntry++;
                
            this->drawSelection();
        break;
        case 1:
            if (direction == 0 && selectedEntry > -1)
                selectedEntry--;
            else if (direction == 1 && selectedEntry < 2)
                selectedEntry++;

            this->drawSelection();
        break;
        case 2:
            if (direction == 0 && selectedEntry > -1)
                selectedEntry--;
            else if (direction == 1 && selectedEntry < 2)
                selectedEntry++;

            this->drawSelection();
        break;
    } 
}
void Menu::action() {
    switch (this->menuMode) {
        case 0: //Main Menu
            if (this->selectedEntry == 0) {
                this->menuMode = 1;
                this->selectedEntry = 0;
            } else if (this->selectedEntry == 1) {
                this->menuMode = 2;
                this->selectedEntry = -1;
            }
            
            this->print();
        break;

        case 1: //start game
            if (this->selectedEntry == -1) {
                npc = 2;
                mode = pong;
            } else if (this->selectedEntry == 0) {
                npc = 1;
                mode = pong;
            } else if (this->selectedEntry == 1) {
                npc = 0;
                mode = pong;
            } else if (this->selectedEntry == 2) {
                this->menuMode = 0;
                this->selectedEntry = 0;
                this->print();
            }

        break;

        case 2: //settings
            if (this->selectedEntry == -1) {
                if (npcDifficulty == 9)
                    npcDifficulty = 3;
                else 
                    npcDifficulty++;
            }

            if (this->selectedEntry == 0) {
                if (winScore == 9)
                    winScore = 2;
                else 
                    winScore++;
            }

            else if (this->selectedEntry == 1) {
                if (speed == 9)
                    speed = 1;
                else 
                    speed++;
            }

            else if (this->selectedEntry == 2) {
                this->menuMode = 0;
                this->selectedEntry = 0;
            }
            this->print();
        break;
    }
}


    /*
     *Player
     *************/
Player::Player(int playerNum) : playerNum(playerNum) {
    if (playerNum == 1)
        this->board[0] = 0;
    else
        this->board[0] = 64 - BOARD_WIDTH;

    this->board[1] = (32 - BOARD_HEIGHT) / 2;
}
void Player::resetBoard() {
    //delete player board
    matrix.fillRect(board[0], 0, BOARD_WIDTH, 32, BLACK);

    //set player board
    if (this->playerNum == 1)
        matrix.fillRect(this->board[0], (32 - BOARD_HEIGHT) / 2, BOARD_WIDTH, BOARD_HEIGHT, WHITE);
    else
        matrix.fillRect(this->board[0], (32 - BOARD_HEIGHT) / 2, BOARD_WIDTH, BOARD_HEIGHT, WHITE);
    
    this->board[1] = (32 - BOARD_HEIGHT) / 2;
}
void Player::moveBoard(int direction) {
    int delRow = -1;
    int addRow = -1;

    if (direction == 0 && this->board[1] >= 0) {
        delRow = this->board[1] + BOARD_HEIGHT;
        addRow = this->board[1];
        this->board[1]--;
    }

    else if (direction == 1 && this->board[1] + BOARD_HEIGHT <= 31) {
        delRow = this->board[1];
        addRow = this->board[1] + BOARD_HEIGHT;
        this->board[1]++;
    }

    matrix.fillRect(this->board[0], delRow, BOARD_WIDTH, 1, BLACK);
    matrix.fillRect(this->board[0], addRow, BOARD_WIDTH, 1, WHITE);
}
void Player::drawScore() {
    //ToDo: Neu implementieren mit gedrehten Zahlen
    if (this->playerNum == 1) {
        matrix.fillRect(24, 1, 5, 7, BLACK);
        matrix.setCursor(24, 1);
    }
    else {
        matrix.fillRect(35, 24, 5, 7, BLACK);
        matrix.setCursor(35, 24);
    }
    matrix.print(this->score);
}
void Player::addPoint() {
    this->score++;
    this->drawScore();
    player1->resetBoard();
    player2->resetBoard();
    ball->resetBall(this->playerNum);
    delay(500);
}
int Player::getScore() {
        return this->score;
}


/*
 *Ball
 *************/
void Ball::move() {
    matrix.drawPixel(this->pos[0], this->pos[1], BLACK);
    if (this->direction == 0) {
        if (this->pos[0] < 62) {
            if (this->angle == 0) {
                this->pos[0]++;
            }
            else if (this->angle > 45 && this->angle <= 75) {
                this->pos[0] += abs(1 / (tan(this->angle * PI / 180.0)));
                this->pos[1]--;
            }
            else if (this->angle <= 45 && this->angle > 0) {
                this->pos[0]++;
                this->pos[1] -= abs(tan(this->angle * PI / 180.0));
            }
            else if (this->angle < 0 && this->angle >= -45) {
                this->pos[0]++;
                this->pos[1] += abs(tan(this->angle * PI / 180.0));
            }
            else if (this->angle < -45 && this->angle >= -75) {
                this->pos[0] += abs(1 / (tan(this->angle * PI / 180.0)));
                this->pos[1]++;
            }
            //TODO: stumpfe Winkel ber 45 Grad
        }
        else {
            if (this->hasHitBoardOfPlayer(player2)) {
                this->calcNewAngle(player2);
                //Move direction-var here, when 'else' is implemented
            }
            else {
                player1->addPoint();
            }
            this->direction = 1;
        }
    }
    else if (this->direction == 1) {
        if (this->pos[0] > 2) {
            if (this->angle == 0) {
                this->pos[0]--;
            }
            else if (this->angle > 45 && this->angle <= 75) {
                this->pos[0] -= abs(1 / (tan(this->angle * PI / 180.0)));
                this->pos[1]--;
            }
            else if (this->angle <= 45 && this->angle > 0) {
                this->pos[0]--;
                this->pos[1] -= abs(tan(this->angle * PI / 180.0));
            }
            else if (this->angle < 0 && this->angle >= -45) {
                this->pos[0]--;
                this->pos[1] += abs(tan(this->angle * PI / 180.0));
            }
            else if (this->angle < -45 && this->angle >= -75) {
                this->pos[0] -= abs(1 / (tan(this->angle * PI / 180.0)));
                this->pos[1]++;
            }
        }
        else {
            if (this->hasHitBoardOfPlayer(player1)) {
                this->calcNewAngle(player1);
            }
            else {
                player2->addPoint();
            }
            this->direction = 0;
        }
    }
    if (this->isAtXBorder()) {
        if (this->angleSwitchLock == 0) {
            this->angle = -this->angle;
            this->angleSwitchLock = 5;
        }

    }
    matrix.drawPixel((int)this->pos[0], (int)this->pos[1], WHITE);
    if (this->pos[0] == 31 || this->pos[0] == 32)
        matrix.fillRect(31, 0, 2, 32, WHITE);

    //TODO: Anders implementieren, wenn score richtig implementiert ist.
    if (this->pos[1] > (0)
        && this->pos[1] < (9)
        && this->pos[0] > (22)
        && this->pos[0] < (30)) {
        player1->drawScore();
    }
    else if (this->pos[1] > (23)
        && this->pos[1] < (32)
        && this->pos[0] > (33)
        && this->pos[0] < (42)) {
        player2->drawScore();
    }


    if (this->angleSwitchLock > 0) {
        this->angleSwitchLock--;
    }

}

bool Ball::isAtXBorder() {
    if (this->pos[1] < 1 || this->pos[1] > 31)
        return true;
    return false;
}

bool Ball::isAtYBorder() {
    if (this->pos[0] < 1 || this->pos[0] > 62)
        return true;
    return false;
}

int Ball::getPosY() {
    return (int)this->pos[1];
}

bool Ball::hasHitBoardOfPlayer(Player* player) {
    if (this->pos[1] >= player->board[1] && this->pos[1] <= (player->board[1] + BOARD_HEIGHT))
        return true;
    return false;
}

void Ball::calcNewAngle(Player* player) {

    //srand(time(NULL));

    int randomNumber = rand() % 4 - 2;

    int playerBoardY = player->board[1];
    if ((int)this->pos[1] == (playerBoardY + ((int)((BOARD_HEIGHT / 2) + 1)))) {
        this->angle = 0 + randomNumber;
        return;
    }
    int angleStep = 75 / (BOARD_HEIGHT / 2);
    for (int i = 0; i < ((int)((BOARD_HEIGHT / 2) + 1)); i++) {
        if ((int)this->pos[1] == (playerBoardY + i)) {
            this->angle = (75 - (i * angleStep)) + randomNumber;
            if (this->angle > 75) {
                this->angle = 75;
            }
            else if (this->angle < -75) {
                this->angle = -75;
            }
            return;
        }
    }
    int mulitplicator = 0;
    for (int i = BOARD_HEIGHT; i > ((int)((BOARD_HEIGHT / 2) + 1)); i--) {
        if ((int)this->pos[1] == (playerBoardY + i)) {
            this->angle = -((75 - (mulitplicator * angleStep)) + randomNumber);
            if (this->angle > 75) {
                this->angle = 75;
            }
            else if (this->angle < -75) {
                this->angle = -75;
            }
            return;
        }
        mulitplicator++;
    }
}

void Ball::resetBall(int playernum) {
    this->pos[1] = 16;
    if (playernum == 1) {
        this->pos[0] = 1;
        this->direction = 0;
    }
    else if (playernum == 2) {
        this->pos[0] = 62;
        this->direction = 1;
    }
    matrix.drawPixel((int)this->pos[0], (int)this->pos[1], WHITE);
}
